![FindMyPhone Logo](http://find-my-phone-api.herokuapp.com/static/img/logo.png)
# Opis aplikacji wraz z instrukcją dot. developmentu oraz kompilacji #

Instrukcja dotyczy dowolnego systemu unixowego z rodziny Debian z zainstalowanym modułem [Perla](https://www.perl.org/) w wersji co najmniej 5.16 oraz przeglądarką internetową (zalecane to [Firefox 33](https://www.mozilla.org/en-US/firefox/new/) lub [Google Chrome](http://www.google.com/chrome)/Chromium 38 )
Instrukcja została pomyślnie przetestowana na surowym systemie Ubuntu 14.10 z przeglądarką Chromium w wersji 38.0.2125.111, 

## Ogólnie o aplikacji ##

Jest to aplikacja kliencka systemu służącego do śledzenia telefonów o nazwie **FindMyPhone**  w postaci strony WWW.

[Demonstracja interfejsu](https://www.appdemostore.com/demo?id=6511034005192704)

[Repozytorium](https://bitbucket.org/zpi16/website/)

[Link do DEMO aplikacji](http://findmyphone.pl/)

Aplikacja:

* jest stworzona w oparciu o skrypty napisane w języku Perl
* wykorzystuje [Mojolicious](http://mojolicio.us/) - web framework dla języka Perl
* jest możliwa do otworzenia w najpopularniejszych przeglądarkach internetowych (Google Chrome, Firefox, Internet Explorer 10+, Opera, Safari)

![Perl Logo](http://upload.wikimedia.org/wikipedia/en/0/00/Perl-camel-small.png)
![Mojolicous Logo](http://mojolicio.us/mojo/logo-white.png)
![Przeglądarki](http://2.bp.blogspot.com/-mwc7F4s5HR4/UmaSO2kdlcI/AAAAAAAAAjQ/4AWLo9LYiBQ/s1600/Digital_20Detective_20NetAnalysis_20Supports_20Mozilla_20Firefox_20-_20Google_20Chrome_20-_20Microsoft_20Internet_20Explorer_20-_20Apple_20Safari_20-_20Opera.png)

## Dostępność aplikacji ##
Aplikacja jest dostępna dla użytkowników pod adresem [findmyphone.pl](http://findmyphone.pl/). Domena prowadzi do wirtualnej instancji Ubuntu postawionej we Frankfurcie w chmurze udostępnianej przez [Amazon Web Services](http://en.wikipedia.org/wiki/Amazon_Web_Services)

## Wymagania przed rozpoczęciem pracy ##

Przed przystąpieniem do wdrażania instrukcji należy sprawdzić aktualną wersję modułu Perl (wymagana conajmniej 5.16):

```
perl -v
```

## Uruchomienie serwera z aplikacją ##

Wejdź do katalogu z aplikacją (zawierający plik index.cgi)

Następnie wykonaj poniższy skrypt

```
perl index.cgi daemon
```

Uruchom przeglądarkę i wpisz w pasku adresu zwrócony adres:

```
Server available at http://127.0.0.1:3000
```

Dla powyższego przypadku będzie to http://127.0.0.1:3000

Każda zmiana plików źródłowych wymaga ponownego uruchomienia serwera w celu zobaczenia zmian.