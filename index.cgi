#!/usr/bin/perl

use lib 'lib/perl5';
use Mojolicious::Lite;
use List::Util 'first';
use JSON;
use Data::Dump 'dump';
my $js = JSON->new->utf8(1);
my $ua = Mojo::UserAgent->new;
my $url = 'http://find-my-phone-api.herokuapp.com/api/v1/';

my $app = app;
$app->sessions->default_expiration(600);
$app->secrets(['ICanHazSecretz']);

sub authorized {
    my ($c) = @_;
    my $token = $c->session->{token};

    if (not defined $token) {
        $c->redirect_to('/login');
    }
    return $token;
}

sub valid_password {
    return length $_[0] && $_[0] =~ /[a-z]/ && $_[0] =~ /\d/;
}

sub valid_email {
    return length $_[0] && $_[0] =~ /^[\w\-][\-\w\.]+[\w\-]  @  \w+(?:\.\w+)*  \.  [a-z0-9]{2,3}$/xi; ;
}

sub partial {
    my ($c, %params) = @_;

    my ($title, $body_class) = delete @params{qw/_title _body_class/};

    $c->render(
        _title      => $title,
        _body_class => $body_class,
        template    => 'main',
        content     => $c->render_to_string(%params),
    );
    return;
}

post '/delete/:id' => sub {
    my ($c)   = @_;
    my $token = authorized($c) or return;
    my $id = $c->param('id');
    if (not $id) {
        $c->redirect_to('/error');
        return;
    }

    my %params;

    my $res = $ua->delete(
        sprintf('%sdevices/%s', $url, $id) => {authorization => $token},
    )->res;

    my $code = $res->code;
    if ($code < 200 || $code > 299) {
       $c->redirect_to('/error');
       return;
   }

    %params = (
        _title => 'You have stoped following the device.',
        _body_class  => 'message',
        message => 'Przestałeś śledzić urządzenie.',
        template => 'message',
    );

    partial($c,%params);

};

get '/devices' => sub {
    my ($c)   = @_;
    my $token = authorized($c) or return;

    my $res = $ua->get(
        $url . 'devices' => {authorization => $token},
    )->res;

    my %params;
    my $code = $res->code;
    if ($code > 199 && $code < 300) {

        my $content = $js->decode( $res->body )->{devices};

        %params = (
            _title => 'Devices',
            _body_class  => 'devices',
            template => 'devices',
            devices => $content,
        );

    } else {
        $c->redirect_to('/error');
        return;
    }

    partial($c, %params);
};

get '/edit/:id' => sub {
    my ($c)   = @_;
    my $token = authorized($c) or return;

    my $id = $c->param('id');
    if (not $id) {
        $c->redirect_to('/error');
        return;
    }

    my $res = $ua->get(
        $url . 'devices' => {authorization => $token},
    )->res;

    my %params;
    my $code = $res->code;
    if ($code < 200 || $code > 299) {
       $c->redirect_to('/error');
       return;
   }

    my $content = $js->decode( $res->body )->{devices};
    my $device = first {$_->{id} == $id} @$content;
    if (not defined $device) {
       $c->redirect_to('/error');
       return;
   }

    %params = (
        _title => 'Edit device',
        _body_class  => 'edit',
        template => 'edit',
        device => $device,
        id => $id,
        validation => {},
    );

    partial($c, %params);
};

post '/edit/:id' => sub {
    my ($c)   = @_;
    my $token = authorized($c) or return;

    my $id = $c->param('id');
    if (not $id) {
        $c->redirect_to('/error');
        return;
    }
    my $device = {
        name => $c->param('name'),
        description => $c->param('description'),
        email => $c->param('email'),
        period => $c->param('period'),
        email_period => $c->param('email_period'),
    };

    my $validation;
    foreach (qw/name email/) {
        if (not defined $device->{$_}) {
            $validation->{$_} = 'Field is required';
        }
    }

    foreach (qw/email period email_period name/) {
        if (not exists $validation->{$_}) {
            $device->{$_} =~ s/^\s+|\s+$//g;
        }
    }

    if (not exists $validation->{name}) {
        $validation->{name} = 'Login is too short' if length $device->{name} < 3;
    }

    if (not exists $validation->{email}) {
        $validation->{email} = 'Provide correct e-mail address'
            unless valid_email($device->{email});
    }

    if (not exists $validation->{email_period}) {
        $validation->{email_period} = 'Minimum 10 seconds'
            if $device->{email_period} < 10;
    }

    my %params;
    if (%$validation) {
        %params = (
            _title => 'Edit device',
            _body_class  => 'edit',
            template => 'edit',
            device => $device,
            validation => $validation,
            id => $id,
        );
    } else {
        my $res = $ua->put(
            sprintf('%sdevices/%s', $url, $id) => {authorization => $token},
            json => {name => $device->{name}, description => $device->{description}},
        )->res;
        my $code = $res->code;
        if ($code < 200 || $code > 299) {
            $c->redirect_to('/error');
            return;
        }

        delete @{$device}{qw/name description/};
        $res = $ua->put(
            sprintf('%sdevices/%s/settings', $url, $id) => {authorization => $token},
            json => $device,
        )->res;
        $code = $res->code;
        if ($code < 200 || $code > 299) {
            $c->redirect_to('/error');
            return;
        }

        %params = (
            _title => 'Device information was changed',
            _body_class  => 'message',
            message => 'Zmieniono dane urządzenia',
            template => 'message'
        );
    }

    partial($c, %params);
};

get '/map/:id' => sub {
    my ($c)   = @_;
    my $token = authorized($c) or return;

    my $id = $c->param('id');
    if (not $id) {
        $c->redirect_to('/error');
        return;
    }

    my $res = $ua->get(
        sprintf('%sdevices/%s/locations', $url, $id) => {authorization => $token},
    )->res;

    my $code = $res->code;
    if ($code < 200 || $code > 299) {
        $c->redirect_to('/error');
        return;
    }

    my $json = $res->body;
    my $locations = $js->decode( $res->body )->{locations};
    my ($d,$m,$y) = ( localtime(time) )[3,4,5];
    my $today = sprintf '%d-%d-%d', 1900+$y, 1+$m, $d;

    foreach (@$locations) {
        ($_->{time}) = $_->{createdAt} =~ /T([^\.]+)/;

        my $date = substr $_->{createdAt}, 0, 10;
        if ($date ne $today) {
            $_->{time} = $date .' '. $_->{time};
        }

        $_->{lat} .= $_->{lat} > 0 ? ' N' : ' S';
        $_->{lng} .= $_->{lng} > 0 ? ' E' : ' W';
    }

    my %params = (
        _title => 'Places',
        _body_class  => 'locations',
        template => 'map',
        locations => $locations,
        json_data => substr($json,13,-1),
        id => $id,
    );

    partial($c, %params);
};

get '/login' => sub {
    my ($c)   = @_;
    my %params = (
        _title => 'Login',
        _body_class  => 'login',
        validation => {},
        template => 'login',
    );

    partial($c, %params);
};

post '/login' => sub {
    my ($c)   = @_;

    my $validation = {};
    my %params = (
        _title => 'Login',
        _body_class  => 'login',
        template => 'login',
    );
    my $template = 'login';

    my %input = (
        email        => $c->param('email'),
        password     => $c->param('password'),
    );

    #czy puste
    foreach (keys %input) {
        $validation->{$_} = 'Field is required' unless length $input{$_};
    }

    #waliduj
    if (!exists $validation->{email}) {
        $input{email} =~ s/^\s+|\s+$//g;
        $validation->{email} = 'Give correct address e-mail' unless valid_email($input{email});
    }

    if ( !exists $validation->{password} && !valid_password($input{password}) ) {
        $validation->{password} = 'Password does not meet requirements!';
    }

    #byly bledy - nie wysylaj do api
    if ( %$validation ) {
        $params{validation} = $validation;
    } else {
        #wyslij do api i sprawdz czy przyszly bledy
        my $response = $ua->post(
            $url . 'users/login', json => {
                password => $input{password},
                email => $input{email},
        })->res;

        my $code = $response->code;

        #sukces, strona informacyjna
        if ($code > 199 && $code < 300) {
            my $token = $response->headers->authorization;

            $c->session(email => $input{email}, token => $token);
            %params = (
                _title => 'Succesful login!',
                _body_class  => 'message',
                message => 'Logowanie zakończone sukcesem',
                template => 'message'
            );
        }
        else {
            my $errors = $js->decode( $response->body )->{errors};
            $params{validation} = $errors;
        }

    }

    partial($c, %params);
};

get '/logout' => sub {
    my ($c) = @_;
    my ($token) = delete @{$c->session}{qw/token email/};
    my %params;

    if (defined $token) {
        %params = (
            _title => 'Succesful sign off!',
            _body_class  => 'message',
            message => 'Wylogowałeś się!',
            template => 'message',
        );
    } else {
        %params =(
            _title => 'Error',
            _body_class  => 'message',
            message => 'Nie jesteś zalogowany!',
            template => 'message',
            status => 403,
        );
    }

    partial($c, %params);
};

get '/register' => sub {
    my ($c)   = @_;
    my %params = (
        _title => 'Register',
        _body_class  => 'register',
        validation => {},
        template => 'register',
    );

    partial($c, %params);
};

post '/register' => sub {
    my ($c)   = @_;

    my $validation = {};
    my %params = (
        _title => 'Register',
        _body_class  => 'register',
        template => 'register',
    );

    my %input = (
        email        => $c->param('email'),
        password     => $c->param('password'),
        confirmation => $c->param('confirmation'),
    );

    #czy puste
    foreach (keys %input) {
        $validation->{$_} = 'Field is required' unless length $input{$_};
    }

    #waliduj
    if (!exists $validation->{email}) {
        $input{email} =~ s/^\s+|\s+$//g;
        $validation->{email} = 'Give correct e-mail address' unless valid_email($input{email});
    }

    if ( !exists $validation->{password} && !valid_password($input{password}) ) {
        $validation->{password} = 'Password does not meet requirements!';
    }

    if (!exists $validation->{confirmation} && $input{password} ne $input{confirmation}) {
        $validation->{confirmation} = 'Passwords are different';
    }

    #byly bledy - nie wysylaj do api
    if ( %$validation ) {
        $params{validation} = $validation;
    } else {
        #wyslij do api i sprawdz czy przyszly bledy
        my $response = $ua->post(
            $url . 'users/register', json => {
                password => $input{password},
                email => $input{email},
        })->res;

        my $code = $response->code;

        #sukces, strona informacyjna
        if ($code > 199 && $code < 300) {
            %params = (
                _title => 'Register - succesful!',
                _body_class  => 'message',
                message => 'Udało się zarejestrować! Przejdź do logowania.',
                template => 'message',
            );
        }
        else {
            my $errors = $js->decode( $response->body )->{errors};
            $params{validation} = $errors;
        }
    }

    partial($c, %params);
};

get '/' => sub {
    my ($c)   = @_;
    my %params = (
        _title => 'Find My Phone',
        _body_class  => 'intro',
        template => 'intro',
    );

    partial($c, %params);
};

# Start the Mojolicious command system
$app->start;
