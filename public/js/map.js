function initialize() {
    var mapOptions = { mapTypeId: google.maps.MapTypeId.ROADMAP };
    var map = new google.maps.Map(document.getElementById("maps"), mapOptions);
    var bounds = new google.maps.LatLngBounds();
    var markers = [];
    var defaultImage = '../images/marker.png';
    var singleImage  = '../images/marker.png';

    showAll()
    map.fitBounds(bounds);
    map.panToBounds(bounds);

    function addMarker(location, name, active, opacity, image) {
        var marker = new google.maps.Marker({
            position: location,
            map: map,
            title: name,
            status: active,
            icon: image || defaultImage,
            opacity: opacity
        });
        return marker;
    }

    //bind showSingle(i) to onclick
    $('tr.loc').each(function(i,el){
        $('a', el).click(function(){

            console.log(i)
            bounds = new google.maps.LatLngBounds();
            markers.forEach(function(el,n) {
                    console.warn(n,el)
                if (n == i) {
                    el.setIcon(singleImage);
                    el.setOpacity(1);
                    el.setVisible(true);
                    bounds.extend(el.getPosition());
                } else {
                    el.setVisible(false)
                }
            })
            map.fitBounds(bounds);
            map.panToBounds(bounds);
        })
    })

    function showAll() {
        bounds = new google.maps.LatLngBounds();
        markers.forEach(function(el) { el.setMap(null) })//clear
            markers = locations.map(function(el,i){
                var ll = new google.maps.LatLng(el.lat, el.lng);
                bounds.extend(ll);
                return addMarker(ll, el.createdAt, "active", i/locations.length);
            })
        map.fitBounds(bounds);
        map.panToBounds(bounds);
    }

    //bind showAll to onclick
    $('.loc-all').click(showAll);

    //finally, build and view markers
    showAll();
}
google.maps.event.addDomListener(window, 'load', initialize);
